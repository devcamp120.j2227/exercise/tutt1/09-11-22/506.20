const express = require("express");
const app = express();
const port = 8000;

const coursesRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

app.use(coursesRouter);
app.use(reviewRouter);

app.listen(port, () => {
    console.log("App listening on port: ", port);
});