const express = require("express");
const router = express.Router();

router.get("/reviews", (req, res) => {
    res.status(200).json({
        message: `Get all reviews`
    });
});

router.get("/reviews/:reviewsId", (req, res) => {
    let reviewsId = req.params.reviewsId;
    res.status(200).json({
        message: `Get reviewsId = ${reviewsId}`
    });
});

router.post("/reviews", (req, res) => {
    res.status(200).json({
        message: `Create reviews`
    });
});

router.put("/reviews/:reviewsId", (req, res) => {
    let reviewsId = req.params.reviewsId;
    res.status(200).json({
        message: `Update reviewsId = ${reviewsId}`
    });
});

router.delete("/reviews/:reviewsId", (req, res) => {
    let reviewsId = req.params.reviewsId;
    res.status(200).json({
        message: `Delete reviewsId = ${reviewsId}`
    });
});

module.exports = router;