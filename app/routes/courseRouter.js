const express = require("express");
const router = express.Router();

router.get("/courses", (req, res) => {
    res.status(200).json({
        message: `Get all Courses`
    });
});

router.get("/courses/:courseId", (req, res) => {
    let courseId = req.params.courseId;
    res.status(200).json({
        message: `Get CoursesId = ${courseId}`
    });
});

router.post("/courses", (req, res) => {
    res.status(200).json({
        message: `Create Course`
    });
});

router.put("/courses/:courseId", (req, res) => {
    let courseId = req.params.courseId;
    res.status(200).json({
        message: `Update CoursesId = ${courseId}`
    });
});

router.delete("/courses/:courseId", (req, res) => {
    let courseId = req.params.courseId;
    res.status(200).json({
        message: `Delete CoursesId = ${courseId}`
    });
});

module.exports = router;